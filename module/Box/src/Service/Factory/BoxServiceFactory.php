<?php
/**
 * &Beyond Sales Engineers (http://www.nbeyond.nl).
 *
 * @copyright Copyright (c) 2017 &Beyond Sales Engineers (http://www.nbeyond.nl)
 */

namespace Box\Service\Factory;

use Interop\Container\ContainerInterface;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

use Box\Service\BoxService;

class BoxServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        // example, get dep. from service manager
        // $myMockupDependency = $container->get('My/Mockup/Dependency');

        return new BoxService(
            //$myMockupDependency
        );
    }

    public function createService(ServiceLocatorInterface $services)
    {
        return $this($services, BoxService::class);
    }
}
